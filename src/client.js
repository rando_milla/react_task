import React from "react";
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Layout from "./components/layout";
import List from "./components/pages/product/list";
import Addnew from "./components/pages/product/addnew";
import Update from "./components/pages/product/update";
import View from "./components/pages/product/view";
const app = document.getElementById('app');
 
ReactDOM.render(
  <Router history={hashHistory}>
    <Route path="/" component={Layout}>
      <IndexRoute component={List}></IndexRoute>
      <Route path="addnew" name="addnew" component={Addnew}></Route>
      <Route path="update" name="update" component={Update}></Route>
      <Route path="view" name="view" component={View}></Route>
    </Route>
  </Router>,
app);
