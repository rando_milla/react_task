import React from "react";
import { Link } from "react-router";
import Nav from "../components/nav/index";
import * as demo from '../data/index';
import styles from "../styles/index.css";
export default class Layout extends React.Component {
  //loading the demo date
  componentDidMount(){
     localStorage.setItem('products',JSON.stringify(demo.demo));
  }
  render() {
    const { location } = this.props;
    
    return (
      <div>
//rendering the menu
        <Nav location={location} />
        <div className={styles.container}>
        <div id="container" class="container">
          <div class="row">
            <div class="col-lg-12">

              {this.props.children}

            </div>
          </div>
          </div>
        </div>
      </div>

    );
  }
}
