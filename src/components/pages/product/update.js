import React from "react";
import * as constants from "../../../utils/constants/index";
export default class Addnew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
                  id: this.props.location.query,
                };
    this.handleInputName = this.handleInputName.bind(this);
    this.handleInputPrice = this.handleInputPrice.bind(this);
    this.handleInputDescription = this.handleInputDescription.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    var products = JSON.parse(localStorage.getItem('products')) || [];
    for (var i=0; i < products.length; i++) {
        if (products[i].id == this.state.id.id) {
            this.state.name= products[i].name;
            this.state.price=products[i].price;
            this.state.description= products[i].description;
          }
    }
  }

  handleInputPrice(e){
    this.setState({price:e.target.value});
  }
  handleInputName(e)
  {
    this.setState({name:e.target.value});
  }
  handleInputDescription(e)
  {
    this.setState({description:e.target.value});
  }
  handleSubmit(event) {
    var current_products = JSON.parse(localStorage.getItem('products')) || [];
    for (var i=0; i < current_products.length; i++) {
        if (current_products[i].id == this.state.id.id) {
            current_products[i].name=this.state.name;
            current_products[i].price=this.state.price;
            current_products[i].description=this.state.description;
          }
    }
  localStorage.setItem('products',JSON.stringify(current_products));
  event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
      <div class="form-group">
        <label for="name">
          {constants.name}:
          </label>
          <input type="text" class="form-control" id="name" value={this.state.name} onChange={this.handleInputName} />
        </div>
        <div class="form-group">
        <label for="price">
        {constants.price}:
        </label>
          <input type="number" class="form-control" id="price" value={this.state.price} onChange={this.handleInputPrice}/>
        </div>
        <div class="form-group">
        <label for="description">
          {constants.description}:
          </label>
          <textarea class="form-control" id="description" value={this.state.description} onChange={this.handleInputDescription}/>
        </div>
        <div class="col-md-12">
        <input type="submit"  class="btn btn-info" value="Submit" />
        </div>
      </form>
    );
  }
}
//validation of props
Addnew.propTypes = {
                     id: React.PropTypes.object
                 };