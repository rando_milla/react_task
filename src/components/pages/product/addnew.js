import React from "react";
import * as constants from "../../../utils/constants/index";
export default class Addnew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
                  id: '',
                  price:'',
                  name:'',
                  description:'',
                  createDate:''
                };
     this.handleInputName = this.handleInputName.bind(this);
     this.handleInputPrice = this.handleInputPrice.bind(this);
     this.handleInputDescription = this.handleInputDescription.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputPrice(e){
    this.setState({price:e.target.value});
  }
  handleInputName(e)
  {
    this.setState({name:e.target.value});
  }
  handleInputDescription(e)
  {
    this.setState({description:e.target.value});
  }
  handleSubmit(event) {
    var Products = JSON.parse(localStorage.getItem('products')) || [];
    this.state.id= Products.length +1;
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    } 

    today = mm+'/'+dd+'/'+yyyy;
    this.state.createDate=today;
    var product_to_enter = { id:this.state.id, price:this.state.price,description:this.state.description,name:this.state.name,createDate:this.state.createDate };
    Products.push(product_to_enter);
    localStorage.setItem('products',JSON.stringify(Products));
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
      <div class="form-group">
        <label for="name">
          {constants.name}:
          </label>
          <input type="text" class="form-control" id="name" onChange={this.handleInputName} />
        </div>
        <div class="form-group">
        <label for="price">
        {constants.price}:
        </label>
          <input type="number" class="form-control" id="price" onChange={this.handleInputPrice}/>
        </div>
        <div class="form-group">
        <label for="description">
          {constants.description}:
          </label>
          <textarea class="form-control" id="description" onChange={this.handleInputDescription}/>
        </div>
        <div class="col-md-12">
        <input type="submit"  class="btn btn-info" value="Submit" />
        </div>
      </form>
    );
  }
}
