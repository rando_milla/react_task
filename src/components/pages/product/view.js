import React from "react";
import * as constants from "../../../utils/constants/index";
export default class View extends React.Component {
  //getting the product
  constructor(props) {
    super(props);
    this.state = {
                  id: this.props.location.query,
                  name:'',
                  price:'',
                  description:'',
                  createDate:'',
                };

    var products = JSON.parse(localStorage.getItem('products')) || [];
    for (var i=0; i < products.length; i++) {
        if (products[i].id == this.state.id.id) {
            this.state.name= products[i].name;
            this.state.price=products[i].price;
            this.state.description= products[i].description;
            this.state.createDate= products[i].createDate;
          }
    } 
  }
  render() {
    //viewing the product
    return (
      <table>
        <tbody>
        <tr>
          <td>{constants.id}</td>
          <td>{this.state.id.id}</td>
        </tr>
        <tr>
        <td>{constants.name}</td>
          <td>{this.state.name}</td>
        </tr>
        <tr>
        <td>{constants.price}</td>
          <td>{this.state.price}</td>
        </tr>
        <tr>
        <td>{constants.description}</td>
          <td>{this.state.description}</td>
        </tr>
        <tr>
          <td>{constants.createDate}</td>
          <td>{this.state.createDate}</td>
        </tr>
        </tbody>
      </table>
    );
  }
}
