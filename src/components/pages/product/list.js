import React from "react";

import Product from "./index";
import * as constants from "../../../utils/constants/index";
import styles from '../../../styles/index.css';
export default class List extends React.Component {
	constructor(props)
	{
		super(props);
	}
	
  render() {
    return (
      <div><ListProducts /></div>
    );
  }
}

function ListProducts(e)
	{
		//getting the products
		const local_products=JSON.parse(localStorage.getItem('products')) || [];
    const Products = local_products.map((product) => <Product key={product.id} id={product.id} name={product.name} price={product.price} description={product.description} createDate={product.createDate} /> );
    return (
    //listing the products
      <div >
        <div class="row">
        	<div class="col-md-12">
		        <table>
		        	<tbody>
		        	<tr>
		        		<th>{constants.id}</th>
		        		<th>{constants.name}</th>
		        		<th>{constants.description}</th>
		        		<th>{constants.price}</th>
		        		<th>{constants.createDate}</th>
		        	</tr>
		        	{Products}
		        	</tbody>
		        </table>
       		</div>
        </div>
      </div>
    );
	}