import React from "react";
import { IndexLink, Link } from "react-router";
import * as constants from "../../../utils/constants/index";
export default class Product extends React.Component {
  constructor(props)
  {
    super(props);
    this.state = {
      id: this.props.id,
      name: this.props.name,
      price: this.props.price,
      description: this.props.description,
      createDate: this.props.createDate,
    }
    this.handleDelete = this.handleDelete.bind(this);
  }
  handleDelete(id)
  {
    const local_products=JSON.parse(localStorage.getItem('products')) || [];
    const new_products = local_products.filter(function(obj)
      {
        if(obj.id!=id.id)
        {
          return true;
        }
        else{
          return false;
        }
      });
    localStorage.setItem('products',JSON.stringify(new_products));
    this.setState({id:'Deleted'});
  }
  render() {
    const id= this.props.id;
    //the view of the product on the list
    return (
      <tr>
        <td>{this.state.id}</td>
        <td>{this.state.name}</td>
        <td>{this.state.description}</td>
        <td>{this.state.price}</td>
        <td>{this.state.createDate}</td>
        <td><button class="btn btn-danger" onClick={()=>this.handleDelete({id})}>{constants.deletion}</button></td>
        <td><Link class="btn btn-success" to={'/update?id='+this.props.id}>{constants.update}</Link></td>
        <td><Link class="btn btn-info" to={'/view?id='+this.props.id}>{constants.view}</Link></td>
      </tr>
    );
  }
}
//validation of props
Product.propTypes = {
   id: React.PropTypes.number,
   name: React.PropTypes.string,
   price: React.PropTypes.number,
   description: React.PropTypes.string,
   createDate: React.PropTypes.date
}
