import React from "react";
import { IndexLink, Link } from "react-router";
import * as constants from "../../utils/constants/index";
export default class Nav extends React.Component {
  constructor() {
    super()
    this.state = {
      collapsed: true,
    };
  }

  toggleCollapse() {
    const collapsed = !this.state.collapsed;
    this.setState({collapsed});
  }

  render() {
    const { location } = this.props;
    const { collapsed } = this.state;
    const listClass = location.pathname === "/" ? "active" : "";
    const addnewClass = location.pathname.match(/^\/addnew/) ? "active" : "";
    const navClass = collapsed ? "collapse" : "";

    return (
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" onClick={this.toggleCollapse.bind(this)} >
              <span class="sr-only">{constants.toggle}</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div class={"navbar-collapse " + navClass} id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class={listClass}>
                <IndexLink to="/" onClick={this.toggleCollapse.bind(this)}>{constants.list}</IndexLink>
              </li>
              <li class={addnewClass}>
                <Link to="addnew" onClick={this.toggleCollapse.bind(this)}>{constants.addNew}</Link>
              </li>

            </ul>
          </div>
        </div>
      </nav>
    );
  }
}
