// fields about the product
export const id="ID";
export const name="Name";
export const price="Price";
export const description="Description";
export const createDate="Creation Date";

//buttons on the list page
export const deletion="Delete";
export const view="View";
export const update="Update";

//menu
export const list="List";
export const addNew="Add new";
export const toggle="Toggle Navigation";